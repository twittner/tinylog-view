-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at http://mozilla.org/MPL/2.0/.

{-# LANGUAGE OverloadedStrings #-}

module System.Logger.View
    ( Element (..)
    , parser
    , render
    ) where

import Control.Applicative
import Data.Monoid
import Data.Text as T
import Data.Attoparsec.Text as A
import Data.Maybe (fromMaybe, mapMaybe)
import System.Console.ANSI
import System.Logger (Level (..))

-----------------------------------------------------------------------------
-- Parsing

data Element = B !Text | L !Level

parser :: Parser [Element]
parser = do
    e <- many' (netstr <|> noise)
    endOfLine
    return $ case e of
        (x:y:z) -> x:fromMaybe y (level y):z -- date, level, ...
        (x:y)   -> fromMaybe x (level x):y   -- level, ...
        xs      -> xs

netstr :: Parser Element
netstr = do
    n <- decimal <* char ':'
    x <- A.take n
    _ <- char ','
    skipWhile isHorizontalSpace
    return (B x)

noise :: Parser Element
noise = B <$> takeWhile1 (not . isEndOfLine)

level :: Element -> Maybe Element
level (B "T")  = Just (L Trace)
level (B "D")  = Just (L Debug)
level (B "I")  = Just (L Info)
level (B "W")  = Just (L Warn)
level (B "E")  = Just (L Error)
level (B "F")  = Just (L Fatal)
level (B  _ )  = Nothing
level x@(L _ ) = Just x

-----------------------------------------------------------------------------
-- Rendering

render :: Bool -> Text -> [Element] -> Text
render c d e =
    let ln = intercalate d . joinFields $ Prelude.map toBytes e
    in if c then colourise ln else ln
  where
    colourise ln = case mapMaybe level e of
        [L Info]  -> colour Green  <> ln <> clear
        [L Warn]  -> colour Yellow <> ln <> clear
        [L Error] -> colour Red    <> ln <> clear
        _         -> ln

    joinFields (x:"=":y:z)
        | x /= "=" && y /= "=" = bold <> x <> normal <> "=" <> y:joinFields z
        | otherwise            = x:"=":y:joinFields z
    joinFields (x:xs)          = x:joinFields xs
    joinFields []              = []

toBytes :: Element -> Text
toBytes (B b)     = b
toBytes (L Trace) = "T"
toBytes (L Debug) = "D"
toBytes (L Info)  = "I"
toBytes (L Warn)  = "W"
toBytes (L Error) = "E"
toBytes (L Fatal) = "F"

colour :: Color -> Text
colour c = pack $ setSGRCode [ SetColor Foreground Vivid c ]

bold, normal, clear :: Text
bold   = pack $ setSGRCode [ SetConsoleIntensity BoldIntensity   ]
normal = pack $ setSGRCode [ SetConsoleIntensity NormalIntensity ]
clear  = pack $ setSGRCode []

