-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at http://mozilla.org/MPL/2.0/.

module Main (main) where

import Control.Monad.IO.Class
import Data.Conduit
import Data.Conduit.Binary
import Data.Conduit.Attoparsec
import Data.Conduit.Text (decodeUtf8)
import Data.Text as T
import Data.Text.IO as T
import Options.Applicative
import System.IO (stdin)
import System.Logger.View

data Opts = Opts
    { colour    :: !Bool
    , delimiter :: !Text
    }

parseOpts :: IO Opts
parseOpts = execParser (info (helper <*> optsParser) desc)
  where
    desc = header "tinylog viewer"
        <> fullDesc
        <> footer "Please be aware that tlview only handles netstrings encoding."

    optsParser = Opts
        <$> (switch $ long "colour"
                <> short 'c'
                <> help "colourise output")
        <*> (fmap pack $ strOption $ long "delimiter"
                <> short 'd'
                <> value ", "
                <> showDefault
                <> help "field delimiter")

main :: IO ()
main = do
    o <- parseOpts
    sourceHandle stdin
        $= decodeUtf8
        $= mapOutput (render (colour o) (delimiter o) . snd) (conduitParser parser)
        $$ awaitForever (liftIO . T.putStrLn)

